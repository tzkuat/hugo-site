# Hugo Automatic Deployment with Docker

## Preriquisites

* Docker

## Gitlab Runner

For this lab, you will need 2 Gitlab runner :
* 1 docker
* 1 shell

I use a shared runner for docker and an on-premise for the shell part

## Stages

* Build hugo container
* Build hugo site with hugo container
* Build Apache container
* Lauch Apache container with Hugo source