+++
title = "À Propos"
date = "2023-04-07"
menu = "main"
+++

Mickael Rigonnaux
============

Formation
---------
---------


2018-2020
:   **Master Cloud, Sécurité & Réseau**; Ynov Informatique (Aix-en-Provence)

    *Mémoire sur la cryptographie dans le système monétique*

2017-2018
:   **Bachelor, Architecte Informatique & SI**; Ynov Informatique (Aix-en-Provence)

    *Rapport sur le déploiement d'un HIDS dans le cadre de la norme PCI DSS*

2015-2017
:   **BTS Services Informatiques aux organisations SISR**; Lycée Laetitia Bonaparte (Ajaccio)


Expériences
----------
----------


Aout 2020 - Aujourd'hui
:   **Administrateur réseau & sécurité**; SITEC (Ajaccio)

    *Architecture réseau LAN & WAN (BGP, OSPF, pare-feu périmétrique) pour deux datacenters, mise en conformité de l'infrastructure, certifications ISO27001 & HDS, gestion des vulnérabilités et des scans de sécurité, gestion de la production, durcissement et écriture de politique de sécurité, accompagnement sécurité dans les projets, gestion IDS / EDR etc.*

Sept. 2017 - Juillet 2020
:   **Ingénieur Cyber Sécurité en alternance**; Monext (Aix-en-Provence)

    *Déploiement d'IDS, gestion des clés cryptographiques (SSL/GPG), sécurité de la partie DevOps (Docker, Kubernetes), sécurité physique (NFC, RFID, Caméras), certification PCI DSS, gestion des vulnérabilités et des alertes (SOC/SIEM), etc.*

Nov. 2018 - Aujourd'hui 
:   **Entrepreneur Indépendant**; Mickael Rigonnaux - RM-IT

    *Installation d'IDS à grande échelle (Wazuh/Suricata), formateur GNU/Linux, sécurité (OSINT) et jury, conseil et expertise en sécurité, relecteur pour le Linux Professional Institute*

Sept. 2018 - Aujourd'hui
:   **Responsable Réseau & Sécurité**; BSI Ynov (Aix-en-Provence)

    *Maintien en conditions opérationnelles de l'infrastructure, tests de sécurité, gestion des flux firewall/internes, installation et configuration des équipements réseaux (Wifi, Firewall, Routeur, Switch), configuration des serveurs (Windows, Linux, VMware), etc.*

Compétences
--------------------
--------------------

**Sécurité**
:

- Sécurité opérationnelle : scan de sécurité (Qualys, Nessus), gestion des vulnérabilités, gestion des configurations et des mises à jour
- Sécurité offensive : base sécurité applicative, OSINT, découverte réseau, recherche de vulnérabilités
- Sécurité défensive : pare-feu, IDS (Wazuh / Suricata / Snort), EDR Sentinel One
- Sensibilisation : campagne de phishing, rédaction, etc.
- Cryptographie : SSL/TLS avancé, GPG/PGP
- Sécurité physique : RFID, NFC, segmentation des zones

**Réseau**
:

- Cisco CCNA1 à 4 : LAN, WAN, VLAN, Routage statique et dynamique, haute disponibilité, ACL
- Firewalling & micro-segmentation : Stormshield, Checkpoint, PFSense/OPNSense 
- Reverse Proxy : HAProxy

**Système**
:

- Administration & Installation de serveurs GNU/Linux : Centos/RedHat, Debian/Ubuntu
- Administration & Installation de serveurs Windows 2008/2019 : Active Directory, GPO, DNS, NPS, DHCP, etc. 
- Virtualisation : VMWare, Proxmox
- Conteneurisation : bonne maîtrise de Docker, base sur Kubernetes

**Autres**
:

- Base en développement : PHP, C#, JavaScript
- Scripting : Python, Bash
- Base de données : SQL (MySQL) & NoSQL (MongoDB)
- Monétique

Hobbies & Loisirs
--------------------
--------------------

- Infrastructure personnelle
- Bug Bounty & CTF 
- Créateur du blog net-security.fr
- Contributeur du blog HomputerSecurity.fr (jusqu'à novembre 2018)




















