+++
title = "Publications"
date = "2023-04-07"
menu = "main"
+++

Liste des différentes publications réalisées
============================================

Mémoire de fin d'études : La cryptographie dans le système monétique
---------------------------------------------------------------------

* [Mémoire format ODT](https://repo.tzku.at/school_rendered/RIGONNAUX-Memory/RIGONNAUX-M%c3%a9moire.odt)
* [Mémoire format PDF](https://repo.tzku.at/school_rendered/RIGONNAUX-Memory/RIGONNAUX-M%c3%a9moire.pdf)
* [Annexes format ODT](https://repo.tzku.at/school_rendered/RIGONNAUX-Memory/RIGONNAUX-Annexes.odt)
* [ANnexes format PDF](https://repo.tzku.at/school_rendered/RIGONNAUX-Memory/RIGONNAUX-Annexes.pdf)


Dossiers écrits et autres
-------------------------

* [Blog Net-Security.fr](https://net-security.fr)
* [Mise en place d'un cluster de firewall PFSense - avec Clément Olier](https://repo.tzku.at/school_rendered/security/MickaelRigonnaux_SecuriteSI_Part_3.pdf)
* [Mise en place d'un firewall PFSense - avec Clément Olier](https://repo.tzku.at/school_rendered/security/MickaelRigonnaux_SecuriteSI_Part_2.pdf)
* [Mise en place d'une attaque ARP](https://repo.tzku.at/school_rendered/security/Rapport%20TP%20Protocole%20ARP%20Olier-Rigonnaux.pdf)
* [Rapport sur les attaques applicatives](https://repo.tzku.at/school_rendered/security/AttaquesApplicatives_Rigonnaux_Olier.pdf)

Présentations
-------------

* [Découverte de l'OSINT (format ODP)](https://repo.tzku.at/presentation/OSINT-tzkuat.odp)
* [Découverte de l'OSINT (format PDF)](https://repo.tzku.at/presentation/OSINT-tzkuat.pdf)
* [Présentation de Tor - avec Fabio Pace (format ODP)](https://repo.tzku.at/presentation/TheOnionReport.odp_1.odp)
* [Présentation de Tor - avec Fabio Pace (format PDF)](https://repo.tzku.at/presentation/TheOnionReport.pdf)
* [Cryptographie (format ODP)](https://repo.tzku.at/presentation/CRYPTO-tzkuat.odp)
* [Cryptographie (format PDF)](https://repo.tzku.at/presentation/CRYPTO-tzkuat.pdf)