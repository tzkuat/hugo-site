+++
title = "Liens à suivre"
date = "2023-04-07"
menu = "main"
+++

## Présentations

Vous trouverez dans cette partie du site des ressources (sites, outils, etc.).

Une instance [Shaarli](https://www.shaarli.fr/) est également disponible sur [ce lien](https://links.tzku.at).

## Liens principaux

* [Serveur Discord OSINT-FR](https://discordapp.com/invite/E2XDKNc)
* [La Quadrature du Net](https://www.laquadrature.net/)
* [Blog Net-Security.fr](https://net-security.fr/)
* [La journal du Hacker](https://www.journalduhacker.net/)
* [Framasoft](https://framasoft.org/fr/)
* [Bellingcat](https://www.bellingcat.com/)

## Par groupe

* [OSINT](/osint)
* [Privacy](/privacy)
* [Security](/security)
* [Awesome List](/awesome-list)
* [Cryptography](/crypto)

## Cheat Sheet 

* [GnuPG](/gpg-cheatsheet)
* [OpenSSL](/openssl-cheatsheet)
