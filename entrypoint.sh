#!/bin/bash

git clone $HUGO_THEME_URL /opt/themes/theme
git clone https://github.com/tzkuat/Ressources /opt/ressources
cp /opt/ressources/*.md /usr/share/blog/content

hugo --themesDir /opt/themes -d /output
